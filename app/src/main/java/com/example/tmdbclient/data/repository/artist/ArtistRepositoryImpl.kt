package com.example.tmdbclient.data.repository.artist

import android.util.Log
import com.example.tmdbclient.data.model.artist.Artist
import com.example.tmdbclient.data.model.artist.ArtistList
import com.example.tmdbclient.data.repository.artist.datasource.ArtistCacheDatasource
import com.example.tmdbclient.data.repository.artist.datasource.ArtistLocalDatasource
import com.example.tmdbclient.data.repository.artist.datasource.ArtistRemoteDatasource
import com.example.tmdbclient.domain.repository.ArtistsRepository
import retrofit2.Response
import java.lang.Exception

class ArtistRepositoryImpl(
    private val artistRemoteDatasource: ArtistRemoteDatasource,
    private val artistCacheDatasource: ArtistCacheDatasource,
    private val artistLocalDatasource: ArtistLocalDatasource
    ) : ArtistsRepository {

    override suspend fun getArtist(): List<Artist>? {

        return getMoviesFromCache()

    }

    override suspend fun updateArtist(): List<Artist>? {
        val newListOfMovies = getMoviesFromAPI()
        artistLocalDatasource.clearAll()
        artistLocalDatasource.saveMoviesToDB(newListOfMovies)
        artistCacheDatasource.saveMovieToCache(newListOfMovies)
        return newListOfMovies
    }

    suspend fun getMoviesFromAPI():List<Artist> {
        lateinit var movieList:List<Artist>
        try {

            val response: Response<ArtistList> = artistRemoteDatasource.getMovies()
            val body : ArtistList? = response.body()
            if (body != null){
                movieList = body.artists
            }

        } catch (exception:Exception){
            Log.i("MyTag",exception.message.toString())
        }
        return movieList
    }

    suspend fun getMoviesFromDB():List<Artist> {
        lateinit var movieList:List<Artist>
        try {

            movieList = artistLocalDatasource.getMoviesFromDB()

        } catch (exception:Exception){
            Log.i("MyTag",exception.message.toString())
        }
        // check movie list in db
        if ( movieList.size > 0) {
            return movieList
        } else {
        // if list in db 0 that means need to get data from api and save to db
            movieList = getMoviesFromAPI()
            artistLocalDatasource.saveMoviesToDB(movieList)
        }

        return movieList
    }

    suspend fun getMoviesFromCache():List<Artist> {
        lateinit var movieList:List<Artist>
        try {

            movieList = artistCacheDatasource.getMoviesFromCaches()

        } catch (exception:Exception){
            Log.i("MyTag",exception.message.toString())
        }
        // check movie list in Cache
        if ( movieList.size > 0) {
            return movieList
        } else {
            // if list in cache 0 that means need to get data from DB and save to Cache
            movieList = getMoviesFromDB()
            artistCacheDatasource.saveMovieToCache(movieList)
        }

        return movieList
    }
    
}