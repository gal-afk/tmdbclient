package com.example.tmdbclient.data.repository.artist.datasource

import com.example.tmdbclient.data.model.artist.Artist

interface ArtistLocalDatasource {

    suspend fun getMoviesFromDB(): List<Artist>
    suspend fun saveMoviesToDB(movies:List<Artist>)
    suspend fun clearAll()
}