package com.example.tmdbclient.data.repository.artist.datasourceimpl

import com.example.tmdbclient.data.model.artist.Artist
import com.example.tmdbclient.data.repository.artist.datasource.ArtistCacheDatasource

class ArtistCacheDatasourceImpl(): ArtistCacheDatasource {

    private var movieList = ArrayList<Artist>()

    override suspend fun getMoviesFromCaches(): List<Artist> {
        return movieList
    }

    override suspend fun saveMovieToCache(movies: List<Artist>) {
        movieList.clear()
        movieList = ArrayList(movies)
    }


}