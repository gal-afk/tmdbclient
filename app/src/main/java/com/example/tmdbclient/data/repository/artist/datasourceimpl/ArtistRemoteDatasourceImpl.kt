package com.example.tmdbclient.data.repository.artist.datasourceimpl

import com.example.tmdbclient.data.api.TMDBService
import com.example.tmdbclient.data.model.artist.ArtistList
import com.example.tmdbclient.data.repository.artist.datasource.ArtistRemoteDatasource
import retrofit2.Response

class ArtistRemoteDatasourceImpl(private val tmbdService: TMDBService, private val apiKey:String):
    ArtistRemoteDatasource {
    // sebagai ganti return menggunakan sama dengan
    override suspend fun getMovies(): Response<ArtistList> = tmbdService.getPopularArtist(apiKey)

}