package com.example.tmdbclient.data.repository.movie

import android.util.Log
import com.example.tmdbclient.data.model.movie.Movie
import com.example.tmdbclient.data.model.movie.MovieList
import com.example.tmdbclient.data.repository.movie.datasource.MovieCacheDatasource
import com.example.tmdbclient.data.repository.movie.datasource.MovieLocalDatasource
import com.example.tmdbclient.data.repository.movie.datasource.MovieRemoteDatasource
import com.example.tmdbclient.domain.repository.MovieRepository
import retrofit2.Response
import java.lang.Exception

class MovieRepositoryImpl(
        private val movieRemoteDatasource: MovieRemoteDatasource,
        private val movieCacheDatasource: MovieCacheDatasource,
        private val movieLocalDatasource: MovieLocalDatasource
    ) : MovieRepository {
    override suspend fun getMovies(): List<Movie>? {
        return getMoviesFromCache()
    }

    override suspend fun updateMovies(): List<Movie>? {
        val newListOfMovies = getMoviesFromAPI()
        movieLocalDatasource.clearAll()
        movieLocalDatasource.saveMoviesToDB(newListOfMovies)
        movieCacheDatasource.saveMovieToCache(newListOfMovies)
        return newListOfMovies
    }

    suspend fun getMoviesFromAPI():List<Movie> {
        lateinit var movieList:List<Movie>
        try {
            val response = movieRemoteDatasource.getMovies()
            val body = response.body()
            if (body != null){
                movieList = body.movies
            }

        } catch (exception:Exception){
            Log.i("MyTag",exception.message.toString())
        }
        return movieList
    }

    suspend fun getMoviesFromDB():List<Movie> {
        lateinit var movieList:List<Movie>
        try {

            movieList = movieLocalDatasource.getMoviesFromDB()

        } catch (exception:Exception){
            Log.i("MyTag",exception.message.toString())
        }
        // check movie list in db
        if ( movieList.size > 0) {
            return movieList
        } else {
        // if list in db 0 that means need to get data from api and save to db
            movieList = getMoviesFromAPI()
            movieLocalDatasource.saveMoviesToDB(movieList)
        }

        return movieList
    }

    suspend fun getMoviesFromCache():List<Movie> {
        lateinit var movieList:List<Movie>
        try {

            movieList = movieCacheDatasource.getMoviesFromCaches()

        } catch (exception:Exception){
            Log.i("MyTag",exception.message.toString())
        }
        // check movie list in Cache
        if ( movieList.size > 0) {
            return movieList
        } else {
            // if list in cache 0 that means need to get data from DB and save to Cache
            movieList = getMoviesFromDB()
            movieCacheDatasource.saveMovieToCache(movieList)
        }

        return movieList
    }
}