package com.example.tmdbclient.data.repository.movie.datasource

import com.example.tmdbclient.data.model.movie.Movie

interface MovieCacheDatasource {

    suspend fun getMoviesFromCaches():List<Movie>
    suspend fun saveMovieToCache(movies:List<Movie>)
}