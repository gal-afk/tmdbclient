package com.example.tmdbclient.data.repository.movie.datasourceimpl

import com.example.tmdbclient.data.db.MovieDao
import com.example.tmdbclient.data.model.movie.Movie
import com.example.tmdbclient.data.repository.movie.datasource.MovieLocalDatasource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MovieLocalDatasourceImpl(private val movieDao: MovieDao): MovieLocalDatasource {

    override suspend fun getMoviesFromDB(): List<Movie> = movieDao.getMovoies()

    // ketika kita ingin mendapatkan query dari room sebenarnya room melakukan background task
    // jadi kita tidak perlu membuatnya hanya cukup memanggilnya
    // menggunakan dispatchers.Io artinya berjalan di workers thread
    override suspend fun saveMoviesToDB(movies: List<Movie>) {
        CoroutineScope(Dispatchers.IO).launch {
            movieDao.saveMovies(movies)
        }
    }

    override suspend fun clearAll() {
        CoroutineScope(Dispatchers.IO).launch {
            movieDao.deleteAllMovies()
        }
    }
}