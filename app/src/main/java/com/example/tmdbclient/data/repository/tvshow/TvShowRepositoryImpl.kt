package com.example.tmdbclient.data.repository.tvshow

import android.util.Log
import com.example.tmdbclient.data.model.tvshow.TvShow
import com.example.tmdbclient.data.model.tvshow.TvShowList
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowCacheDatasource
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowLocalDatasource
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowRemoteDatasource
import com.example.tmdbclient.domain.repository.TvShowsRepository
import retrofit2.Response
import java.lang.Exception

class TvShowRepositoryImpl(
    private val tvShowRemoteDatasource: TvShowRemoteDatasource,
    private val tvShowLocalDatasource: TvShowLocalDatasource,
    private val tvShowCacheDatasource: TvShowCacheDatasource
    ):TvShowsRepository {
    override suspend fun getTvShow(): List<TvShow>? {
        TODO("Not yet implemented")
    }

    override suspend fun updateTvShow(): List<TvShow>? {
        TODO("Not yet implemented")
    }

    suspend fun getMoviesFromAPI():List<TvShow> {
        lateinit var movieList:List<TvShow>
        try {

            val response: Response<TvShowList> = tvShowRemoteDatasource.getMovies()
            val body : TvShowList? = response.body()
            if (body != null){
                movieList = body.tvShows
            }

        } catch (exception: Exception){
            Log.i("MyTag",exception.message.toString())
        }
        return movieList
    }

    suspend fun getMoviesFromDB():List<TvShow> {
        lateinit var movieList:List<TvShow>
        try {

            movieList = tvShowLocalDatasource.getMoviesFromDB()

        } catch (exception: Exception){
            Log.i("MyTag",exception.message.toString())
        }
        // check movie list in db
        if ( movieList.size > 0) {
            return movieList
        } else {
        // if list in db 0 that means need to get data from api and save to db
            movieList = getMoviesFromAPI()
            tvShowLocalDatasource.saveMoviesToDB(movieList)
        }

        return movieList
    }

    suspend fun getMoviesFromCache():List<TvShow> {
        lateinit var movieList:List<TvShow>
        try {

            movieList = tvShowCacheDatasource.getMoviesFromCaches()

        } catch (exception: Exception){
            Log.i("MyTag",exception.message.toString())
        }
        // check movie list in Cache
        if ( movieList.size > 0) {
            return movieList
        } else {
            // if list in cache 0 that means need to get data from DB and save to Cache
            movieList = getMoviesFromDB()
            tvShowCacheDatasource.saveMovieToCache(movieList)
        }

        return movieList
    }
}