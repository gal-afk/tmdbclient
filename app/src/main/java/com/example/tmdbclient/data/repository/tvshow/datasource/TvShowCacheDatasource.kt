package com.example.tmdbclient.data.repository.tvshow.datasource

import com.example.tmdbclient.data.model.movie.Movie
import com.example.tmdbclient.data.model.tvshow.TvShow

interface TvShowCacheDatasource {
    suspend fun getMoviesFromCaches():List<TvShow>
    suspend fun saveMovieToCache(movies:List<TvShow>)
}