package com.example.tmdbclient.data.repository.tvshow.datasource

import com.example.tmdbclient.data.model.movie.Movie
import com.example.tmdbclient.data.model.tvshow.TvShow

interface TvShowLocalDatasource {

    suspend fun getMoviesFromDB(): List<TvShow>
    suspend fun saveMoviesToDB(movies:List<TvShow>)
    suspend fun clearAll()
}