package com.example.tmdbclient.data.repository.tvshow.datasourceimpl

import com.example.tmdbclient.data.model.movie.Movie
import com.example.tmdbclient.data.model.tvshow.TvShow
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowCacheDatasource

class TvShowCacheDatasourceImpl():TvShowCacheDatasource {
    private var movieList = ArrayList<TvShow>()

    override suspend fun getMoviesFromCaches(): List<TvShow> {
        return movieList
    }

    override suspend fun saveMovieToCache(movies: List<TvShow>) {
        movieList.clear()
        movieList = ArrayList(movies)
    }

}