package com.example.tmdbclient.domain.repository

import com.example.tmdbclient.data.model.artist.Artist
import com.example.tmdbclient.data.model.movie.Movie

interface ArtistsRepository {

    suspend fun getArtist():List<Artist>?
    suspend fun updateArtist():List<Artist>?
}