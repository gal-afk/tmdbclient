package com.example.tmdbclient.presentation.di.core

import com.example.tmdbclient.data.repository.artist.datasource.ArtistCacheDatasource
import com.example.tmdbclient.data.repository.artist.datasourceimpl.ArtistCacheDatasourceImpl
import com.example.tmdbclient.data.repository.movie.datasource.MovieCacheDatasource
import com.example.tmdbclient.data.repository.movie.datasourceimpl.MovieCacheDatasourceImpl
import com.example.tmdbclient.data.repository.tvshow.datasourceimpl.TvShowCacheDatasourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class CacheDataModule {

    @Provides
    @Singleton
    fun provideMovieCacheDataSource():MovieCacheDatasource{
        return MovieCacheDatasourceImpl()
    }

    @Provides
    @Singleton
    fun provideTvCacheDataSource(): TvShowCacheDatasourceImpl {
        return TvShowCacheDatasourceImpl()
    }

    @Provides
    @Singleton
    fun provideArtistCacheDataSource(): ArtistCacheDatasource {
        return ArtistCacheDatasourceImpl()
    }
}