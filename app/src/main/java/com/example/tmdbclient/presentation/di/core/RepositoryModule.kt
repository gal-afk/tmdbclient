package com.example.tmdbclient.presentation.di.core

import com.example.tmdbclient.data.repository.artist.ArtistRepositoryImpl
import com.example.tmdbclient.data.repository.artist.datasource.ArtistCacheDatasource
import com.example.tmdbclient.data.repository.artist.datasource.ArtistLocalDatasource
import com.example.tmdbclient.data.repository.artist.datasource.ArtistRemoteDatasource
import com.example.tmdbclient.data.repository.movie.MovieRepositoryImpl
import com.example.tmdbclient.data.repository.movie.datasource.MovieCacheDatasource
import com.example.tmdbclient.data.repository.movie.datasource.MovieLocalDatasource
import com.example.tmdbclient.data.repository.movie.datasource.MovieRemoteDatasource
import com.example.tmdbclient.data.repository.tvshow.TvShowRepositoryImpl
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowCacheDatasource
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowLocalDatasource
import com.example.tmdbclient.data.repository.tvshow.datasource.TvShowRemoteDatasource
import com.example.tmdbclient.domain.repository.ArtistsRepository
import com.example.tmdbclient.domain.repository.MovieRepository
import com.example.tmdbclient.domain.repository.TvShowsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun provideMovieRepository(
            movieLocalDatasource: MovieLocalDatasource,
            movieRemoteDatasource: MovieRemoteDatasource,
            movieCacheDatasource: MovieCacheDatasource
    ): MovieRepository {
        return MovieRepositoryImpl(
                movieRemoteDatasource ,
                movieCacheDatasource,
                movieLocalDatasource
        )
    }
    @Singleton
    @Provides
    fun provideTvShowRepository(
            tvShowLocalDatasource: TvShowLocalDatasource,
            tvShowRemoteDatasource: TvShowRemoteDatasource,
            tvShowCacheDatasource: TvShowCacheDatasource
    ):TvShowsRepository{
        return TvShowRepositoryImpl(
                tvShowRemoteDatasource,
                tvShowLocalDatasource,
                tvShowCacheDatasource
        )
    }
    @Singleton
    @Provides
    fun provideArtistRepository(
            artistCacheDatasource: ArtistCacheDatasource,
            artistLocalDatasource: ArtistLocalDatasource,
            artistRemoteDatasource: ArtistRemoteDatasource
    ):ArtistsRepository{
        return ArtistRepositoryImpl(
                artistRemoteDatasource,
                artistCacheDatasource,
                artistLocalDatasource
        )
    }


}