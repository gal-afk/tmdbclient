package com.example.tmdbclient.presentation.di.core

import com.example.tmdbclient.domain.repository.ArtistsRepository
import com.example.tmdbclient.domain.repository.MovieRepository
import com.example.tmdbclient.domain.repository.TvShowsRepository
import com.example.tmdbclient.domain.usecase.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Singleton
    @Provides
    fun provideGetMovieUseCase(movieRepository: MovieRepository):GetMoviesUseCase{
        return GetMoviesUseCase(movieRepository)
    }

    @Singleton
    @Provides
    fun provideUpdateMovieUseCase(movieRepository: MovieRepository):UpdateMoviesUseCase{
        return UpdateMoviesUseCase(movieRepository)
    }

    @Singleton
    @Provides
    fun provideGetTvShowUseCase(tvShowsRepository: TvShowsRepository):GetTvShowUseCase{
        return GetTvShowUseCase(tvShowsRepository)
    }

    @Singleton
    @Provides
    fun provideUpdateTvShowUseCase(tvShowsRepository: TvShowsRepository):UpdateTvShowUseCase{
        return UpdateTvShowUseCase(tvShowsRepository)
    }

    @Singleton
    @Provides
    fun provideGetArtistUseCase(artistsRepository: ArtistsRepository):GetArtistUseCase{
        return GetArtistUseCase(artistsRepository)
    }

    @Singleton
    @Provides
    fun provideUpdateArtistUseCase(artistsRepository: ArtistsRepository):UpdateArtistsUseCase{
        return UpdateArtistsUseCase(artistsRepository)
    }

}