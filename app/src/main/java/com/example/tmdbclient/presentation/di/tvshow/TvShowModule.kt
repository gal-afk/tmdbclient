package com.example.tmdbclient.presentation.di.tvshow

import com.example.tmdbclient.domain.usecase.*
import com.example.tmdbclient.presentation.artist.ArtistViewModelFactory
import com.example.tmdbclient.presentation.di.artist.ArtistScope
import com.example.tmdbclient.presentation.movie.MovieViewModelFactory
import com.example.tmdbclient.presentation.tvshow.TvShowViewModelFactory
import dagger.Module
import dagger.Provides


@Module
class TvShowModule {

    @TvShowScope
    @Provides
    fun provideTvShowViewModelFactory(getTvShowUseCase: GetTvShowUseCase,updateTvShowUseCase: UpdateTvShowUseCase): TvShowViewModelFactory {
        return TvShowViewModelFactory(getTvShowUseCase,updateTvShowUseCase)
    }
}